package de.xxlcreeper.io;

public abstract class DirectoryChangeListener {

	public abstract void changed(WatchResult result);
	
}
