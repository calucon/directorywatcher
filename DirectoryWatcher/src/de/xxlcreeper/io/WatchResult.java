package de.xxlcreeper.io;

import java.io.File;

public class WatchResult {

	private File file;
	private WatcherKey key;
	
	protected WatchResult(File file, WatcherKey key){
		this.file = file;
		this.key = key;
	}
	
//------------------------------------------
	
	/**
	 * 
	 * @return The WatcherKey which indicates what has happened to the File
	 */
	public WatcherKey getKey(){
		return this.key;
	}
	
	/**
	 * 
	 * @return The File that is involved
	 * 		Can return an Exception if the File got deleted
	 */
	public File getFile(){
		return this.file;
	}
	
}
