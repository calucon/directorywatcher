# DirectoryWatcher #

The DirectoryWatcher observes a single Directory and notifies you when a file/directory is added/changed/removed.

### How does it work? ###


```
#!java
File dir_to_watch = new File("path/to/directory");
DirectoryWatcher watcher = new DirectoryWatcher(dir_to_watch);

for(WatchResult result : watcher.getResults()){
    //Do something
}
```
If you need constant updates you can either use your own Thread or use the built in mechanism:
```
#!java
watcher.addChangeListner(new DirectoryChangeListener() {			
    @Override
    public void changed(WatchResult result) {
        //Do something
    }
});
```